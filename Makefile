CC=gcc
DEBUG = -Wall -Werror
CFLAGS = -c -std=c11 -O2 $(DEBUG)
LDFLAGS = $(DEBUG)
SOURCES = main.c
OBJECTS = $(SOURCES:.c=.o)
EXECUTABLE = tana.exe

all: $(SOURCES) $(EXECUTABLE)

debug: DEBUG+=-g -DDEBUG
debug: all

clean:
	find . -name "*~" -type f -delete
	find . -name "*.o" -type f -delete

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

.c.o:
	$(CC) $(CFLAGS) $< -o $@
