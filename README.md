# tana
## What is tana?
tana (all lowercase) is a small [golfing](https://en.wikipedia.org/wiki/Code_golf) language that is based on 4-bit opcodes.

## How to get tana?
`git clone` the [repository](https://gitlab.com/_njdm/tana/), adjust the Makefile (in case you run on a superior OS than Windows like... anything else), and just run `make all`.

## How to code in a 4-bit system?
It's simple. Just fire up your favorite hex editor and get started (you may want to write a small script that handles this for you though).

## How to run my code?
Navigate to the folder your code is in and execute tana as followed: `tana <filename>`.
Ofcourse if tana is not in your path (it should be, it's an important tool for any real programmer after all) you need to specify the path to the executable.

## Licensing
View [LICENCE](LICENSE) for more info.

## About the author
tana is a side project by me. It's probably going to be discontinued. If that's the case feel free to pick it up.
If you find yourself having questions or finding a bug feel free to open an issue or send an email to <mailto:njdm.evil@gmail.com>.
