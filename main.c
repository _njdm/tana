#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TANA_VERSION "0.0.0"

int main(int argc, char *argv[])
{
  // Help
  if (argc < 2 || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
    printf("Usage: %s [-h | --help] [-v | --version] <file>", argv[0]);
    return 0;
  }

  // Version
  if (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version")) {
    printf("tana v" TANA_VERSION " by _njdm\n");
    return 0;
  }

  // Open file
  FILE *fp = fopen(argv[1], "rb");
  if (!fp) {
    perror("Failed to open file");
    return -1;
  }

  // Get file size
  fseek(fp, 0l, SEEK_END);
  long fileSize = ftell(fp);
  if (fileSize == -1) {
    perror("Error getting file size");
    fclose(fp);
    return -1;
  }
  rewind(fp);

  // 0 byte message
  if (fileSize == 0) {
    printf("%s\n", argv[1]);
    fclose(fp);
    return 0;
  }

  // Read code
  uint8_t *code = calloc(2 * fileSize + 1, sizeof(uint8_t));
  if (!code) {
    perror("Failed to allocate memory");
    fclose(fp);
    return -1;
  }

  {
    int8_t currentByte;
    int32_t i = 0;
    while ((currentByte = fgetc(fp)) != EOF) {
      code[i++] = ((uint8_t)currentByte) >> 4;
      code[i++] = currentByte & 0x0f;
    }
  }

  if (ferror(fp)) {
    perror("Failed to read file");
    fclose(fp);
    free(code);
  }

  fclose(fp);

  // TODO: Execute code

  free(code);

  return 0;
}
